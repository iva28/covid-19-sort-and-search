from tkinter import *
import tkinter.ttk as ttk
import csv
from operator import itemgetter as array

root = Tk()
root.title("Region Covid-19")
width = 700
height = 500
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()
x = (screen_width/2) - (width/2)
y = (screen_height/2) - (height/2)
root.geometry("%dx%d+%d+%d" % (width, height, x, y))
root.resizable(0, 0)

#setting for GUI table
TableMargin = Frame(root, width=700)
TableMargin.pack(side=TOP)
scrollbarx = Scrollbar(TableMargin, orient=HORIZONTAL)
scrollbary = Scrollbar(TableMargin, orient=VERTICAL)
tree = ttk.Treeview(TableMargin, columns=('Code','Province','City','Latitude','Longitude','Elementary_School_Count','Kindergarten_Count','University_Count','Academy_Ratio','Elderly_Population_Ratio','Elderly_Alone_Ratio','Nursing_Home_Count'), height=400, selectmode="extended", yscrollcommand=scrollbary.set, xscrollcommand=scrollbarx.set)
scrollbary.config(command=tree.yview)
scrollbary.pack(side=RIGHT, fill=Y)
scrollbarx.config(command=tree.xview)
scrollbarx.pack(side=BOTTOM, fill=X)
tree.heading('Code', text="Code", anchor=W)
tree.heading('Province', text="Province", anchor=W)
tree.heading('City', text="City", anchor=W)
tree.heading('Latitude', text="Latitude", anchor=W)
tree.heading('Longitude', text="Longitude", anchor=W)
tree.heading('Elementary_School_Count', text="Elementary School Count", anchor=W)
tree.heading('Kindergarten_Count', text="Kindergarten Count", anchor=W)
tree.heading('University_Count', text="University Count", anchor=W)
tree.heading('Academy_Ratio', text="Academy Ratio", anchor=W)
tree.heading('Elderly_Population_Ratio', text="Elderly Population Ratio", anchor=W)
tree.heading('Elderly_Alone_Ratio', text="Elderly Alone Ratio", anchor=W)
tree.heading('Nursing_Home_Count', text="Nursing Home Count", anchor=W)
tree.column('#0', stretch=NO, minwidth=0, width=0)
tree.column('#1', stretch=NO, minwidth=0, width=70)
tree.column('#2', stretch=NO, minwidth=0, width=150)
tree.column('#3', stretch=NO, minwidth=0, width=150)
tree.column('#4', stretch=NO, minwidth=0, width=150)
tree.column('#5', stretch=NO, minwidth=0, width=150)
tree.column('#6', stretch=NO, minwidth=0, width=200)
tree.column('#7', stretch=NO, minwidth=0, width=140)
tree.column('#8', stretch=NO, minwidth=0, width=130)
tree.column('#9', stretch=NO, minwidth=0, width=130)
tree.column('#10', stretch=NO, minwidth=0, width=200)
tree.column('#11', stretch=NO, minwidth=0, width=200)
tree.column('#12', stretch=NO, minwidth=0, width=200)
tree.pack()

#sorting
with open('Databases/Region.csv') as case, open('Databases/sort_region.csv', 'w') as sort:
    writer = csv.writer(sort, delimiter=',')
    reader = csv.reader(case, delimiter=',')
    next(reader)
    sorting = sorted(reader, key=lambda row: row[5], reverse=True)
    for row in sorting:
        writer.writerow(row)
#insert sorted data into treeview
with open('Databases/sort_region.csv') as f:
    field = ['code', 'province', 'city', 'latitude', 'longitude', 'elementary_school_count', 'kindergarten_count', 'university_count', 'academy_ratio','elderly_population_ratio','elderly_alone_ratio','nursing_home_count']
    reader = csv.DictReader(f, fieldnames=field)
    for row in reader:
        code = row['code']
        province = row['province']
        city = row['city']
        latitude = row['latitude']
        longitude = row['longitude']
        elementary_school_count = row['elementary_school_count']
        kindergarten_count = row['kindergarten_count']
        university_count = row['university_count']
        academy_ratio = row['academy_ratio']
        elderly_population_ratio = row['elderly_population_ratio']
        elderly_alone_ratio = row['elderly_alone_ratio']
        nursing_home_count = row['nursing_home_count']
        tree.insert("", 0, values=(code,province,city,latitude,longitude,elementary_school_count,kindergarten_count,university_count,academy_ratio,elderly_population_ratio,elderly_alone_ratio,nursing_home_count))

#============================INITIALIZATION==============================
if __name__ == '__main__':
    root.mainloop()
