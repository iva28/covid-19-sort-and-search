import csv

data=[]
with open("Databases/PatientInfo.csv") as f:
    reading = csv.reader(f)
    for row in reading:
        data.append(row)

birth_year = input("Enter a birth year : ")

col = [x[3] for x in data]

if birth_year in col:
    for x in range(0, len(data)):
        if birth_year == data[x][3]:
            print(data[x])

else:
    print("Birth year doesn't exist")
