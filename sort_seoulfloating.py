from tkinter import *
import tkinter.ttk as ttk
import csv
from operator import itemgetter as array

root = Tk()
root.title("Seoul Floating Covid-19")
width = 700
height = 500
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()
x = (screen_width/2) - (width/2)
y = (screen_height/2) - (height/2)
root.geometry("%dx%d+%d+%d" % (width, height, x, y))
root.resizable(0, 0)

#setting for GUI table
TableMargin = Frame(root, width=700)
TableMargin.pack(side=TOP)
scrollbarx = Scrollbar(TableMargin, orient=HORIZONTAL)
scrollbary = Scrollbar(TableMargin, orient=VERTICAL)
tree = ttk.Treeview(TableMargin, columns=('Date','Hour','Birth_Year','Sex','Province','City','Fp_Num'), height=400, selectmode="extended", yscrollcommand=scrollbary.set, xscrollcommand=scrollbarx.set)
scrollbary.config(command=tree.yview)
scrollbary.pack(side=RIGHT, fill=Y)
scrollbarx.config(command=tree.xview)
scrollbarx.pack(side=BOTTOM, fill=X)
tree.heading('Date', text="Date", anchor=W)
tree.heading('Hour', text="Hour", anchor=W)
tree.heading('Birth_Year', text="Birth Year", anchor=W)
tree.heading('Sex', text="Sex", anchor=W)
tree.heading('Province', text="Province", anchor=W)
tree.heading('City', text="City", anchor=W)
tree.heading('Fp_Num', text="Fp Num", anchor=W)

tree.column('#0', stretch=NO, minwidth=0, width=0)
tree.column('#1', stretch=NO, minwidth=0, width=100)
tree.column('#2', stretch=NO, minwidth=0, width=100)
tree.column('#3', stretch=NO, minwidth=0, width=100)
tree.column('#4', stretch=NO, minwidth=0, width=100)
tree.column('#5', stretch=NO, minwidth=0, width=100)
tree.column('#6', stretch=NO, minwidth=0, width=120)
tree.column('#7', stretch=NO, minwidth=0, width=100)
tree.pack()

#sorting
with open('Databases/SeoulFloating.csv') as case, open('Databases/sort_seoulfloating.csv', 'w') as sort:
    writer = csv.writer(sort, delimiter=',')
    reader = csv.reader(case, delimiter=',')
    next(reader)
    sorting = sorted(reader, key=lambda row: row[0], reverse=True)
    for row in sorting:
        writer.writerow(row)
#insert sorted data into treeview
with open('Databases/sort_seoulfloating.csv') as f:
    field = ['date','hour','birth_year','sex','province','city','fp_num']
    reader = csv.DictReader(f, fieldnames=field)
    for row in reader:
        date = row['date']
        hour = row['hour']
        birth_year = row['birth_year']
        sex = row['sex']
        province = row['province']
        city = row['city']
        fp_num = row['fp_num']
        tree.insert("", 0, values=(date,hour,birth_year,sex,province,city,fp_num))

#============================INITIALIZATION==============================
if __name__ == '__main__':
    root.mainloop()
