import csv

data=[]
with open("Databases/PatientRoute.csv") as f:
    reading = csv.reader(f)
    for row in reading:
        data.append(row)

global_num = input("Enter a global num: ")

col = [x[1] for x in data]

if global_num in col:
    for x in range(0, len(data)):
        if global_num == data[x][1]:
            print(data[x])

else:
    print("Global num doesn't exist")
