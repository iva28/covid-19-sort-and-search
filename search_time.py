import csv

data=[]
with open("Databases/Time.csv") as f:
    reading = csv.reader(f)
    for row in reading:
        data.append(row)

date = input("Enter a date: ")

col = [x[0] for x in data]

if date in col:
    for x in range(0, len(data)):
        if date == data[x][0]:
            print(data[x])

else:
    print("Date doesn't exist")
