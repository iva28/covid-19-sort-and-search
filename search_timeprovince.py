import csv

data=[]
with open("Databases/TimeProvince.csv") as f:
    reading = csv.reader(f)
    for row in reading:
        data.append(row)

province = input("Enter a province: ")

col = [x[2] for x in data]

if province in col:
    for x in range(0, len(data)):
        if province == data[x][2]:
            print(data[x])

else:
    print("Province doesn't exist")
