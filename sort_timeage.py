from tkinter import *
import tkinter.ttk as ttk
import csv
from operator import itemgetter as array

root = Tk()
root.title("Time Age Covid-19")
width = 500
height = 500
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()
x = (screen_width/2) - (width/2)
y = (screen_height/2) - (height/2)
root.geometry("%dx%d+%d+%d" % (width, height, x, y))
root.resizable(0, 0)

#setting for GUI table
TableMargin = Frame(root, width=700)
TableMargin.pack(side=TOP)
scrollbarx = Scrollbar(TableMargin, orient=HORIZONTAL)
scrollbary = Scrollbar(TableMargin, orient=VERTICAL)
tree = ttk.Treeview(TableMargin, columns=('Date','Time','Age','Confirmed','Deceased'), height=400, selectmode="extended", yscrollcommand=scrollbary.set, xscrollcommand=scrollbarx.set)
scrollbary.config(command=tree.yview)
scrollbary.pack(side=RIGHT, fill=Y)
scrollbarx.config(command=tree.xview)
scrollbarx.pack(side=BOTTOM, fill=X)
tree.heading('Date', text="Date", anchor=W)
tree.heading('Time', text="Time", anchor=W)
tree.heading('Age', text="Age", anchor=W)
tree.heading('Confirmed', text="Confirmed", anchor=W)
tree.heading('Deceased', text="Deceased", anchor=W)

tree.column('#0', stretch=NO, minwidth=0, width=0)
tree.column('#1', stretch=NO, minwidth=0, width=100)
tree.column('#2', stretch=NO, minwidth=0, width=100)
tree.column('#3', stretch=NO, minwidth=0, width=100)
tree.column('#4', stretch=NO, minwidth=0, width=100)
tree.column('#5', stretch=NO, minwidth=0, width=100)
tree.pack()

#sorting
with open('Databases/TimeAge.csv') as case, open('Databases/sort_timeage.csv', 'w') as sort:
    writer = csv.writer(sort, delimiter=',')
    reader = csv.reader(case, delimiter=',')
    next(reader)
    sorting = sorted(reader, key=lambda row: row[2], reverse=True)
    for row in sorting:
        writer.writerow(row)
#insert sorted data into treeview
with open('Databases/sort_timeage.csv') as f:
    field = ['date','time','age','confirmed','deceased']
    reader = csv.DictReader(f, fieldnames=field)
    for row in reader:
        date = row['date']
        time = row['time']
        age = row['age']
        confirmed = row['confirmed']
        deceased = row['deceased']
        tree.insert("", 0, values=(date,time,age,confirmed,deceased))

#============================INITIALIZATION==============================
if __name__ == '__main__':
    root.mainloop()
