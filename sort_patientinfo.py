from tkinter import *
import tkinter.ttk as ttk
import csv
from operator import itemgetter as array

root = Tk()
root.title("Patient Info Covid-19")
width = 700
height = 500
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()
x = (screen_width/2) - (width/2)
y = (screen_height/2) - (height/2)
root.geometry("%dx%d+%d+%d" % (width, height, x, y))
root.resizable(0, 0)

#setting for GUI table
TableMargin = Frame(root, width=700)
TableMargin.pack(side=TOP)
scrollbarx = Scrollbar(TableMargin, orient=HORIZONTAL)
scrollbary = Scrollbar(TableMargin, orient=VERTICAL)
tree = ttk.Treeview(TableMargin, columns=('Patient_Id', 'Global_Num', 'Sex', 'Birth_Year', 'Age', 'Country', 'Province', 'City', 'Disease','Infection_Case','Infection_Order','Infected_By','Contact_Number','Symptom_Onset_Date','Confirmed_Date','Released_Date','Deceased_Date','State'), height=400, selectmode="extended", yscrollcommand=scrollbary.set, xscrollcommand=scrollbarx.set)
scrollbary.config(command=tree.yview)
scrollbary.pack(side=RIGHT, fill=Y)
scrollbarx.config(command=tree.xview)
scrollbarx.pack(side=BOTTOM, fill=X)
tree.heading('Patient_Id', text="Patient ID", anchor=W)
tree.heading('Global_Num', text="Global Num", anchor=W)
tree.heading('Sex', text="Sex", anchor=W)
tree.heading('Birth_Year', text="Birth Year", anchor=W)
tree.heading('Age', text="Age", anchor=W)
tree.heading('Country', text="Country", anchor=W)
tree.heading('Province', text="Province", anchor=W)
tree.heading('City', text="City", anchor=W)
tree.heading('Disease', text="Disease", anchor=W)
tree.heading('Infection_Case', text="Infection Case", anchor=W)
tree.heading('Infection_Order', text="Infection Order", anchor=W)
tree.heading('Infected_By', text="Infected By", anchor=W)
tree.heading('Contact_Number', text="Contact Number", anchor=W)
tree.heading('Symptom_Onset_Date', text="Symptom Onset Date", anchor=W)
tree.heading('Confirmed_Date', text="Confirmed Date", anchor=W)
tree.heading('Released_Date', text="Released Date", anchor=W)
tree.heading('Deceased_Date', text="Deceased Date", anchor=W)
tree.heading('State', text="State", anchor=W)
tree.column('#0', stretch=NO, minwidth=0, width=0)
tree.column('#1', stretch=NO, minwidth=0, width=90)
tree.column('#2', stretch=NO, minwidth=0, width=90)
tree.column('#3', stretch=NO, minwidth=0, width=60)
tree.column('#4', stretch=NO, minwidth=0, width=80)
tree.column('#5', stretch=NO, minwidth=0, width=35)
tree.column('#6', stretch=NO, minwidth=0, width=60)
tree.column('#7', stretch=NO, minwidth=0, width=140)
tree.column('#8', stretch=NO, minwidth=0, width=120)
tree.column('#9', stretch=NO, minwidth=0, width=70)
tree.column('#10', stretch=NO, minwidth=0, width=250)
tree.column('#11', stretch=NO, minwidth=0, width=150)
tree.column('#12', stretch=NO, minwidth=0, width=180)
tree.column('#13', stretch=NO, minwidth=0, width=150)
tree.column('#14', stretch=NO, minwidth=0, width=200)
tree.column('#15', stretch=NO, minwidth=0, width=120)
tree.column('#16', stretch=NO, minwidth=0, width=120)
tree.column('#17', stretch=NO, minwidth=0, width=120)
tree.column('#18', stretch=NO, minwidth=0, width=90)
tree.pack()

#sorting
with open('Databases/PatientInfo.csv') as case, open('Databases/sort_patientinfo.csv', 'w') as sort:
    writer = csv.writer(sort, delimiter=',')
    reader = csv.reader(case, delimiter=',')
    next(reader)
    sorting = sorted(reader, key=lambda row: row[6], reverse=True)
    for row in sorting:
        writer.writerow(row)
#insert sorted data into treeview
with open('Databases/sort_patientinfo.csv') as f:
    field = ['patient_id', 'global_num', 'sex', 'birth_year', 'age', 'country', 'province', 'city', 'disease','infection_case','infection_order','infected_by','contact_number','symptom_onset_date','confirmed_date','released_date','deceased_date','state']
    reader = csv.DictReader(f, fieldnames=field)
    for row in reader:
        patient_id = row['patient_id']
        global_num = row['global_num']
        sex = row['sex']
        birth_year = row['birth_year']
        age = row['age']
        country = row['country']
        province = row['province']
        city = row['city']
        disease = row['disease']
        infection_case = row['infection_case']
        infection_order = row['infection_order']
        infected_by = row['infected_by']
        contact_number = row['contact_number']
        symptom_onset_date = row['symptom_onset_date']
        confirmed_date = row['confirmed_date']
        released_date = row['released_date']
        deceased_date = row['deceased_date']
        state = row['state']
        tree.insert("", 0, values=(patient_id,global_num,sex,birth_year,age,country,province,city,disease,infection_case,infection_order,infected_by,contact_number,symptom_onset_date,confirmed_date,released_date,deceased_date,state))

#============================INITIALIZATION==============================
if __name__ == '__main__':
    root.mainloop()
